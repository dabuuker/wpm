# Requirements

Composer

WP-CLI (Recommended as a global composer require with ~/.composer/vendor/bin added to PATH)

Deployer ( https://deployer.org/ )

## On Windows
Cygwin (http://www.cygwin.com/) for rsync and ssh (add cygwin_dir/bin to path after installation)

Also, modify "../etc/fstab" under cygwin's install directory with "none /cygdrive cygdrive binary,posix=0,user,noacl 0 0"
to fix permissions related issues ( https://www.itefix.net/content/permissions-filesdirectories-are-clutteredmixed )