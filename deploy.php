<?php
namespace Deployer;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Deployer\Task\Context;

// Configuration

serverList('servers.yml');
argument('stage', InputArgument::OPTIONAL, 'Run tasks only on this server or group of servers');

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('rsync_excludes',[
    'wp-config.php',
    '.git',
    '.gitignore',
    'deploy.php',
    'servers.yml',
    'servers.example.yml',
    '/vendor',
    '*.sql',
    'composer.json',
    'composer.lock',
    'wp-cli.phar',
    'wp-content/themes/**/node_modules',
    'wp_content/themes/**/.git',
    'wp-content/themes/**/bower_components',
    'wp-content/themes/**/.gitignore',
    'backup'
]);

set('keep_backups',5);

// Tasks

task('wp:new',[
    'wp:download',
    'wp:config:local',
    'wp:db_create',
    'wp:install'
]);

task('wp:cli',function(){
    if(remoteFileExists('wp-cli.phar')){
        writeln('wp-cli.phar exists, skipping...');
    } else {
        run('cd {{deploy_path}} && curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar');
        write(run('cd {{deploy_path}} && php wp-cli.phar --info'));
    }
});

task('wp:download','wp core download');

task('wp:config:remote',function(){
    if(remoteFileExists('wp-config.php')){
        writeln('wp-config.php already exists, skipping...');
    } else {
        run('cd {{deploy_path}} && php wp-cli.phar core config --dbname="{{dbname}}" --dbuser="{{dbuser}}" --dbhost="{{dbhost}}" --dbpass="{{dbpassword}}"');
    }
});

task('wp:config:local',function(){
    if(file_exists('wp-config.php')){
        writeln('wp-config.php already exists, skipping...');
    }
    else {
        runLocally('wp core config --dbname="'.getInEnv('dbname','dev').'" --dbuser="'.getInEnv('dbuser','dev').'"');
    }
});

task('wp:db_create','wp db create');
task('wp:install','wp core install --url="{{url}}" --title="{{title}}" --admin_user="{{adminuser}}" --admin_password="{{adminpass}}" --admin_email="{{adminemail}}"');

task('push:files',function(){
    $conf = Context::get()->getServer()->getConfiguration();
    $user = $conf->getUser();
    $host = $conf->getHost();
    $to = $user . '@' . $host . ':{{deploy_path}}/';

    writeln(runLocally(rsyncCmdString('./',$to)));
});

task('push:db',function(){
    // Create temp db dump
    runLocally('wp db export db.sql');

    upload('db.sql','{{deploy_path}}/db.sql');
    
    writeln('Importing db...');
    run('cd {{deploy_path}} && php wp-cli.phar db import db.sql');
    run('cd {{deploy_path}} && php wp-cli.phar search-replace '. getInEnv('url','dev') .' {{url}}'); 

    //cleanup
    writeln('Cleaning up...');
    runLocally('rm db.sql');
    run('rm {{deploy_path}}/db.sql');
});

task('pull:db',function(){
    run('cd {{deploy_path}} && php wp-cli.phar db export db.sql');
    download('db.sql','{{deploy_path}}/db.sql');

    writeln('Importing db..');
    runLocally('wp db import db.sql');
    runLocally('wp search-replace {{url}} ' . getInEnv('url','dev'));

    writeln('Cleaning up..');
    runLocally('rm db.sql');
    run('rm {{deploy_path}}/db.sql');
});

task('pull:files',function(){
    $conf = Context::get()->getServer()->getConfiguration();
    $user = $conf->getUser();
    $host = $conf->getHost();
    $from = $user . '@' . $host . ':{{deploy_path}}/';
    
    $r = runLocally(rsyncCmdString($from,'./'));
    writeln($r);
});


// Grouped Tasks
task('push',['push:files','wp:cli','wp:config:remote','push:db']);
task('pull',['pull:files','wp:config:local','pull:db']);

// Functions

function rsyncCmdString($from, $to){
    $conf = Context::get()->getServer()->getConfiguration();
    $port = $conf->getPort();
    $user = $conf->getUser();
    $host = $conf->getHost();
    $password = $conf->getPassword();
    $identityMethod = $conf->getPemFile() ? 'getPemFile' : $conf->getPrivateKey() ? 'getPrivateKey' : false;
    $identityFile = $identityMethod ? ' -i ' . $conf->$identityMethod()  : '';
    
    $cmd = 'rsync --chmod=Du=rwx,Dgo=rx,Fu=rw,Fog=r -rzcvE -e ';
    $cmd .= "'ssh" . $identityFile . " -p " . $port ."' ";
    $cmd .= getRsyncExcludeString() . ' ';
    $cmd .= $from . ' ';
    $cmd .= $to;

    return $cmd;
}

function remoteFileExists($file){
    return test('[ -f {{deploy_path}}/'.$file.' ]');
}

function getInEnv($key,$env){
    return Deployer::get()->environments->get($env)->get($key);
}

function getServerName(){
    $server = Context::get()->getServer();
    return $server->getConfiguration()->getName();
}

function getRsyncExcludeString(){
    $excludeString = '';
    foreach(get('rsync_excludes') as $exclude){
        $excludeString .= '--exclude "' . $exclude . '" ';
    }
    return $excludeString;
}